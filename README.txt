Bash script to draw ASCII boxes and arrows.

Usage:

    dumbox <command1> <text1> [<command2> <text2> ...]

Commands:

    1. Boxes
      box   - draws a small box with the text in it
      Box   - draws a big box
      LBox  - draws a big box with a left connector
      RBox  - draws a big box with a right connector
      LRBox - draws a big box with a left and a right connector

    2. Arrows
      LTarrow  - draws an right to left arrow with a label on the top
      LBarrow  - draws an right to left arrow with a label on the bottom
      RTarrow  - draws an left to right arrow with a label on the top
      RBarrow  - draws an left to right arrow with a label on the bottom
      LRTarrow - draws an double arrow with a label on the top
      LRBarrow - draws an double arrow with a label on the bottom

Example:

    $ dumbox RBox "Wonderful" LRTarrow "Hello world" LBox "Boxes"
    +-----------+               +-------+
    |           |  Hello world  |       |
    | Wonderful +<------------->+ Boxes |
    |           |               |       |
    +-----------+               +-------+

To draw multiline graphs, call the program several times. Vertical arrows
are not supported. Feel free to send patches.

Depends on:

 - bash (uses process substitution <(...) bashism).
 - paste
 - sed

Under the WTFPL License (FLOSS).
